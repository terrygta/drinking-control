// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package drink.domain;

import drink.domain.Beverage;
import drink.domain.Drinker;
import drink.domain.Log;
import java.util.Date;

privileged aspect Log_Roo_JavaBean {
    
    public Date Log.getDrinkDate() {
        return this.drinkDate;
    }
    
    public void Log.setDrinkDate(Date drinkDate) {
        this.drinkDate = drinkDate;
    }
    
    public int Log.getAmount() {
        return this.amount;
    }
    
    public void Log.setAmount(int amount) {
        this.amount = amount;
    }
    
    public Drinker Log.getDrinker() {
        return this.drinker;
    }
    
    public void Log.setDrinker(Drinker drinker) {
        this.drinker = drinker;
    }
    
    public Beverage Log.getBeverage() {
        return this.beverage;
    }
    
    public void Log.setBeverage(Beverage beverage) {
        this.beverage = beverage;
    }
    
}
