package drink.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PlanUnit {

    @NotNull
    @Min(0L)
    private int amount;

    @NotNull
    @Min(1L)
    @Max(366L)
    private short dayIndex;

    @NotNull
    @ManyToOne
    private Plan plan;

    @NotNull
    @ManyToOne
    private Beverage beverage;
    
    public static List<PlanUnit> findPlanUnitsByPlan(Plan plan) {
    	EntityManager em = PlanUnit.entityManager();
        TypedQuery<PlanUnit> q = em.createQuery("SELECT o FROM PlanUnit AS o WHERE o.plan = :plan ORDER BY o.dayIndex ASC", PlanUnit.class);
        q.setParameter("plan", plan);
    	return q.getResultList();
    }
    
    @Transactional
    public void removeByPlan(Plan plan) {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
        	EntityManager em = PlanUnit.entityManager();
            Query q = em.createQuery("DELETE FROM PlanUnit WHERE plan = :plan");
            q.setParameter("plan",plan).executeUpdate();
        }
    }
}
