package drink.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Log {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date drinkDate;

    @NotNull
    @Min(0L)
    private int amount;

    @ManyToOne
    private Drinker drinker;

    @NotNull
    @ManyToOne
    private Beverage beverage;
    
    public static Log findLogByDrinkerAndId(Long id, Drinker drinker) {
        if (id == null || drinker == null) return null;
        EntityManager em = Log.entityManager();
        TypedQuery<Log> q = em.createQuery("SELECT o FROM Log AS o WHERE o.drinker = :drinker and o.id = :id", Log.class);
        q.setParameter("drinker", drinker);
        q.setParameter("id", id);
        return q.getSingleResult();
    }
    
    public static List<Log> findLogEntriesByDrinker(int firstResult, int maxResults, Drinker drinker) {
    	EntityManager em = Log.entityManager();
        TypedQuery<Log> q = em.createQuery("SELECT o FROM Log AS o WHERE o.drinker = :drinker ORDER BY o.drinkDate DESC", Log.class);
        q.setParameter("drinker", drinker);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
    	return q.getResultList();
    }
    
    public static List<Log> findAllLogsByDrinker(Drinker drinker) {
    	EntityManager em = Log.entityManager();
        TypedQuery<Log> q = em.createQuery("SELECT o FROM Log AS o WHERE o.drinker = :drinker ORDER BY o.drinkDate DESC", Log.class);
        q.setParameter("drinker", drinker);
    	return q.getResultList();
    }
    
    public static long countLogsByDrinker(Drinker drinker) {
    	EntityManager em = Log.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Log o WHERE o.drinker = :drinker", Long.class);
        q.setParameter("drinker", drinker);
        return q.getSingleResult();
    }
    
    @Transactional
    public void removeByBeverage(Beverage beverage) {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
        	EntityManager em = Log.entityManager();
            Query q = em.createQuery("DELETE FROM Log WHERE beverage = :beverage");
            q.setParameter("beverage",beverage).executeUpdate();       
        }
    }
}
