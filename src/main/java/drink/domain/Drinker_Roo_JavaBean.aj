// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package drink.domain;

import drink.domain.Drinker;
import drink.domain.Profile;

privileged aspect Drinker_Roo_JavaBean {
    
    public String Drinker.getUsername() {
        return this.username;
    }
    
    public void Drinker.setUsername(String username) {
        this.username = username;
    }
    
    public String Drinker.getPassword() {
        return this.password;
    }
    
    public void Drinker.setPassword(String password) {
        this.password = password;
    }
    
    public Profile Drinker.getProfile() {
        return this.profile;
    }
    
    public void Drinker.setProfile(Profile profile) {
        this.profile = profile;
    }
    
}
