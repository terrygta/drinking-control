package drink.domain;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPlansByDrinker" })
public class Plan {

	@NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @ManyToOne
    private Drinker drinker;
    
    public static Plan findPlanByDrinker(Drinker drinker) {
        if (drinker == null) return null;
        EntityManager em = Plan.entityManager();
        TypedQuery<Plan> q = em.createQuery("SELECT o FROM Plan o WHERE o.drinker = :drinker", Plan.class);
        q.setParameter("drinker", drinker);
        return q.getSingleResult();
    }
    
    public static long countPlansByDrinker(Drinker drinker) {
    	EntityManager em = Plan.entityManager();
    	TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Plan o WHERE o.drinker = :drinker", Long.class);
    	q.setParameter("drinker", drinker);
    	return q.getSingleResult();
    }
}
