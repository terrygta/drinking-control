// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package drink.domain;

import drink.domain.Drinker;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Drinker_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Drinker.entityManager;
    
    public static final EntityManager Drinker.entityManager() {
        EntityManager em = new Drinker().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Drinker.countDrinkers() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Drinker o", Long.class).getSingleResult();
    }
    
    public static List<Drinker> Drinker.findAllDrinkers() {
        return entityManager().createQuery("SELECT o FROM Drinker o", Drinker.class).getResultList();
    }
    
    public static Drinker Drinker.findDrinker(Long id) {
        if (id == null) return null;
        return entityManager().find(Drinker.class, id);
    }
    
    public static List<Drinker> Drinker.findDrinkerEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Drinker o", Drinker.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Drinker.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Drinker.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Drinker attached = Drinker.findDrinker(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Drinker.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Drinker.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Drinker Drinker.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Drinker merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
