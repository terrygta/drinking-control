package drink.web;

import drink.domain.Drinker;
import drink.domain.Log;
import drink.domain.Plan;
import drink.domain.PlanUnit;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/plans")
@Controller
@RooWebScaffold(path = "plans", formBackingObject = Plan.class)
public class PlanController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Plan plan, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
		String username = principal.getName();
		if (username == null || bindingResult.hasErrors()) {
            populateEditForm(uiModel, plan);
            return "plans/create";
        }
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if(Plan.countPlansByDrinker(drinker) > 0)
		{
			return "plans/current";
		}
		float REFACTOR_NUM = (float) 0.9;
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(plan.getStartDate());
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add(Calendar.DAY_OF_MONTH, 6);
        plan.setEndDate(endDate.getTime());
        uiModel.asMap().clear();       
        plan.setDrinker(drinker);
        plan.persist();
        List<Log> logs = Log.findLogEntriesByDrinker(0, 20, drinker);
        Calendar end = Calendar.getInstance();
        end.setTime(logs.get(0).getDrinkDate());
        end.add(Calendar.DAY_OF_MONTH, -7);
        for(int i = 0; i < logs.size(); i++)
        {
        	Calendar date = Calendar.getInstance();
        	date.setTime(logs.get(i).getDrinkDate());
        	if(date.equals(end))
        		break;
        	PlanUnit unit = new PlanUnit();
        	unit.setPlan(plan);
        	int dayIndex = date.get(Calendar.DAY_OF_WEEK) - startDate.get(Calendar.DAY_OF_WEEK) + 1;
        	if(dayIndex <= 0)
        		dayIndex = dayIndex + 7;
        	unit.setDayIndex((short) dayIndex);
        	unit.setBeverage(logs.get(i).getBeverage());
        	unit.setAmount((int) (logs.get(i).getAmount() * REFACTOR_NUM));
        	unit.persist();
        }
        return "plans/current";
    }

	void populateEditForm(Model uiModel, Plan plan) {
        uiModel.addAttribute("plan", plan);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if(Plan.countPlansByDrinker(drinker) > 0)
		{
			Plan plan = Plan.findPlanByDrinker(drinker);
			PlanUnit unit = new PlanUnit();
			unit.removeByPlan(plan);
			plan.remove();
		}
        populateEditForm(uiModel, new Plan());
        return "plans/create";
    }
	
	@RequestMapping(value = "/getPlan", produces = "text/html")
    public String createPlanPage(Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if(Plan.countPlansByDrinker(drinker) > 0)
			return "plans/current";
		return "redirect:/plans?form";
    }
	
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
    public @ResponseBody ModelMap getData(Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		Plan plan = Plan.findPlanByDrinker(drinker);
		List<PlanUnit> units = PlanUnit.findPlanUnitsByPlan(plan);
		Calendar startDate = Calendar.getInstance();
        startDate.setTime(plan.getStartDate());
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Date");
		col.addAttribute("type", "date");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Beverage Name");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());
		col.addAttribute("label", "Beverage ABV (%)");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());
		col.addAttribute("label", "Amount (ml)");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		for(int i = 0; i < units.size(); i++)
		{
			ModelMap row = new ModelMap();
			List<ModelMap> cells = new ArrayList<ModelMap>();
			ModelMap cell = new ModelMap();
			Calendar date = (Calendar) startDate.clone();
			date.add(Calendar.DAY_OF_MONTH, units.get(i).getDayIndex() - 1);
			StringBuilder sb = new StringBuilder();
			sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
			append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).
			append(")");
			cell.addAttribute("v", sb.toString());
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", units.get(i).getBeverage().getName());
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", units.get(i).getBeverage().getVolume().toString());
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", units.get(i).getAmount());
			cells.add((ModelMap) cell.clone());
			row.addAttribute("c", cells);
			rows.add(row);
		}
		data.addAttribute("rows", rows);
		return data;
    }
}
