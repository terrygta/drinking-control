package drink.web;

import drink.domain.Beverage;
import drink.domain.Drinker;
import drink.domain.Log;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RequestMapping("/beverages")
@Controller
@RooWebScaffold(path = "beverages", formBackingObject = Beverage.class)
public class BeverageController {

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Beverage());
        return "beverages/create";
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Beverage beverage, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
		String username = principal.getName();
		if (username == null || bindingResult.hasErrors()) {
            populateEditForm(uiModel, beverage);
            return "beverages/create";
        }
		
        beverage.setDrinker(Drinker.findDrinkerByUsernameEquals(username));
        uiModel.asMap().clear();
        beverage.persist();
        return "redirect:/beverages/" + encodeUrlPathSegment(beverage.getId().toString(), httpServletRequest);
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Beverage beverage, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
		String username = principal.getName();
		if (username == null || bindingResult.hasErrors()) {
            populateEditForm(uiModel, beverage);
            return "beverages/update";
        }
		beverage.setDrinker(Drinker.findDrinkerByUsernameEquals(username));
        uiModel.asMap().clear();
        beverage.merge();
        return "redirect:/beverages/" + encodeUrlPathSegment(beverage.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
        uiModel.addAttribute("beverage", Beverage.findBeverageByDrinkerAndId(id, drinker));
        uiModel.addAttribute("itemId", id);
        return "beverages/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, Principal principal) {
		
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("beverages", Beverage.findBeverageEntriesByDrinker(firstResult, sizeNo, drinker));
            float nrOfPages = (float) Beverage.countBeveragesByDrinker(drinker) / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("beverages", Beverage.findAllBeveragesByDrinker(drinker));
        }
        return "beverages/list";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Beverage beverage = Beverage.findBeverage(id);
        Log log = new Log();
        log.removeByBeverage(beverage);
        beverage.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/beverages";
    }
}
