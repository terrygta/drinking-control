package drink.web;

import drink.domain.Drinker;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/drinkers")
@Controller
@RooWebScaffold(path = "drinkers", formBackingObject = Drinker.class, delete=false)
public class DrinkerController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Drinker drinker, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, drinker);
            return "drinkers/create";
        }
        uiModel.asMap().clear();
        drinker.persist();
        return "drinkers/success";
    }
}
